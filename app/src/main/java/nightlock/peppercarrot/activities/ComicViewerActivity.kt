/*
 * Copyright (C) 2017 - 2019 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.activities

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import kotlinx.android.synthetic.main.activity_comic_viewer.*
import nightlock.peppercarrot.R
import nightlock.peppercarrot.adapters.ComicViewerAdapter
import nightlock.peppercarrot.utils.ArchiveDataManager
import nightlock.peppercarrot.utils.Episode
import nightlock.peppercarrot.utils.Language

class ComicViewerActivity : AppCompatActivity() {
    private val hideHandler = Handler()

    private var visibility: Boolean = false
    private val hideRunnable = Runnable { hide() }

    private val delayHideTouch = {
        delayedHide(AUTO_HIDE_DELAY_MILLIS)
        false
    }

    private var episodeId = 0
    private val language by lazy {
        intent.getParcelableExtra<Language>(Language.LANGUAGE)
    }
    private val episodeDb by lazy {
        ArchiveDataManager(this)
    }

    private lateinit var comicViewerAdapter: ComicViewerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comic_viewer)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        visibility = true

        button_back.setOnClickListener {
            comic_viewpager.setCurrentItem(comic_viewpager.currentItem - 1, true)
            delayHideTouch()
        }
        button_forward.setOnClickListener {
            comic_viewpager.setCurrentItem(comic_viewpager.currentItem + 1, true)
            delayHideTouch()
        }
        button_forward.setOnLongClickListener {
            if (episodeId < episodeDb.length() - 1) {
                episodeId++
                attachAdapter(episodeDb.get(episodeId)!!, language!!)

                Toast
                        .makeText(it.context, R.string.jumped_to_next_episode, Toast.LENGTH_SHORT)
                        .show()
            }
            delayHideTouch()
            true
        }
        button_back.setOnLongClickListener {
            if (episodeId > 1) {
                episodeId--
                attachAdapter(episodeDb.get(episodeId)!!, language!!)

                Toast.makeText(it.context, R.string.jumped_to_previous_episode, Toast.LENGTH_SHORT
                ).show()
            }
            delayHideTouch()
            true
        }

        val episode = intent.getParcelableExtra<Episode>(Episode.EPISODE)
        episodeId = episode?.index ?: 0
        attachAdapter(episode, language)
    }

    private fun attachAdapter(episode: Episode?, language: Language?) {
        comicViewerAdapter = ComicViewerAdapter(episode!!, language!!, supportFragmentManager)
        comicViewerAdapter.notifyDataSetChanged()
        comic_viewpager.adapter = comicViewerAdapter
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        delayedHide(100)
    }

    override fun onResume() {
        super.onResume()
        delayedHide(100)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun toggle() {
        if (visibility) hide()
        else {
            show()
            delayHideTouch()
        }
    }

    private fun hide() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.run {
                hide(WindowInsets.Type.navigationBars())
                hide(WindowInsets.Type.statusBars())
                hide(WindowInsets.Type.ime())
                systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            }
            window.setDecorFitsSystemWindows(false)
        } else {
            val visibilitySetting = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION

            comic_viewpager.systemUiVisibility = visibilitySetting
            window.decorView.systemUiVisibility = visibilitySetting

            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }
        fullscreen_content_controls.visibility = View.GONE

        visibility = false
    }

    private fun show() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.run {
                show(WindowInsets.Type.navigationBars())
                show(WindowInsets.Type.statusBars())
                show(WindowInsets.Type.ime())
            }
            window.setDecorFitsSystemWindows(false)
        } else {
            val visibilitySettings = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION

            comic_viewpager.systemUiVisibility = visibilitySettings
            window.decorView.systemUiVisibility = visibilitySettings

            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        fullscreen_content_controls.visibility = View.VISIBLE

        visibility = true
    }

    private fun delayedHide(delayMillis: Int) {
        hideHandler.removeCallbacks(hideRunnable)
        hideHandler.postDelayed(hideRunnable, delayMillis.toLong())
    }

    companion object {
        private const val AUTO_HIDE_DELAY_MILLIS = 2500
    }
}
